import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Modal from "@mui/material/Modal";
import RegisterUserForm from "./RegisterForm";
import { useEffect, useState } from "react";
import LoginUserForm from "./LoginForm"
import { useLocation, useNavigate } from "react-router-dom";
//import { useSelector } from "react-redux";
import { Alert, Snackbar } from "@mui/material";
import Typography from '@mui/material/Typography';
import RegisterForm from "./RegisterForm";
import LoginForm from "./LoginForm";


const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 500,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 4,
};

export default function AuthModel({ handleClose, open }) {
   const location = useLocation();
  // const { auth } = useSelector((store) => store);
  // const navigate=useNavigate()
  // useEffect(() => {
  //   if (auth.user){
  //      handleClose();
  //      if(auth.user?.role==="ADMIN"){
  //       navigate('/admin')
  //      }
  //     }
  // }, [auth.user]);
  return (
    <>
    <Modal
      open={open}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
      size="large"
    >
      <Box className="rounded-md" sx={style}>
        {location.pathname === "/login" ? (
          <LoginForm />
        ) : (
          <RegisterForm />
        )}
      
        {/* <Typography id="modal-modal-description" sx={{mt:2}}>ddddddddddddddddd</Typography> */}
      </Box>
    </Modal>
    
    </>
    
  );
}
