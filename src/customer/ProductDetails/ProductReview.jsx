import React from "react";
import { Avatar } from "@mui/material";
import { Rating, Box,  Grid } from "@mui/material";

const ProductReview = () => {
//   const [value, setValue] = React.useState(4.5);
  return (
    <div className="">
      <Grid container spacing={2} gap={3}>
        <Grid item xs={1}>
          <Box>
            <Avatar
              className="text-white"
              sx={{ width: 56, height: 56, bgcolor: "#9155FD" }}
            //   alt={item?.user?.firstName}
            //   src=""
            r
            >
              {/* {item?.user?.firstName[0].toUpperCase()} */}
            </Avatar>
          </Box>
        </Grid>
        <Grid item xs={9}>
          <div className="space-y-2">
            <div className="">
              <p className="font-semibold text-xl">ram</p>
              <p className="opacity-70 text-lg">April 5, 2023</p>
            </div>
            <div>
            

              <Rating
                value={4.5}
                // onChange={(event, newValue) => {
                //   setValue(newValue);
                // }}
                name="half-rating"
                defaultValue={2.5}
                precision={0.5}
                readOnly 
                
              />
             
            </div>
            <p className="text-lg">
              {/* {item?.review} */}nice product
            </p>
          </div>
        </Grid>
      </Grid>
      <div className="col-span-1 flex"></div>
    </div>
  );
};

export default ProductReview;
