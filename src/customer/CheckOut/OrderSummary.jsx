import React, { useEffect } from "react";
import { Button } from "@mui/material";
import AddressCard from "../AddressCard/AddressCard";
import CartItem from "../Cart/CartItem";
import { useDispatch, useSelector } from "react-redux";
import { getOrderById } from "../../State/Order/Action";
import { useLocation } from "react-router-dom";
import { createPayment } from "../../State/Payment/Action";
import api from "../../config/apiConfig";

const OrderSummary = () => {
  const { order } = useSelector((store) => store);
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const orderId = searchParams.get("order_id");
  const dispatch = useDispatch();

  console.log("orderId ", order);
  useEffect(() => {
    dispatch(getOrderById(orderId));
  }, [orderId]);

  const handleCreatePayment =async () => {
    const data = order?.order?._id ;
    dispatch(createPayment(data));
    console.log("link",data)
  };

  // const handleCreatePayment = async () => {
  //   try {
  //     const orderId = order?.order?._id; // Ensure order ID is correctly accessed
  //     if (!orderId) {
  //       console.error('Order ID is missing');
  //       return;
  //     }

  //     const response = await api.post(`/api/payments/${orderId}`);
  //     console.log("Create payment response:", response.data);

  //     if (response.data && response.data.payment_link_url) {
  //       // setPaymentLink(response.data.payment_link_url);
  //     } else {
  //       console.error('No payment link URL found in response:', response);
  //     }
  //   } catch (error) {
  //     console.error('Error creating payment link:', error);
  //   }
  // };


  
  

  return (
    <div className="space-y-5">
      <div className="p-5 shadow-lg rounded-md border ">
        <AddressCard address={order.order?.shippingAddress} />
      </div>
      <div className="lg:grid grid-cols-3 relative justify-between">
        <div className="lg:col-span-2 ">
          {order.order?.orderItems.map((item) => (
            <CartItem key={item._id} item={item} />
          ))}
        </div>
        <div className="sticky top-0 h-[100vh] mt-5 lg:mt-0 ml-5 text-lg">
          <div className="border p-5 bg-white shadow-lg rounded-md">
            <p className="font-bold opacity-60 pb-4">PRICE DETAILS</p>
            <hr />

            <div className="space-y-3 font-semibold">
              <div className="flex justify-between pt-3 text-black ">
                <span>Price ({order.order?.totalItem} item)</span>
                <span>₹{order.order?.totalPrice}</span>
              </div>
              <div className="flex justify-between">
                <span>Discount</span>
                <span className="text-green-700">
                  -₹{order.order?.discounte}
                </span>
              </div>
              <div className="flex justify-between">
                <span>Delivery Charges</span>
                <span className="text-green-700">Free</span>
              </div>
              <hr />
              <div className="flex justify-between font-bold text-lg">
                <span>Total Amount</span>
                <span className="text-green-700">
                  ₹{order.order?.totalDiscountedPrice}
                </span>
              </div>
            </div>

            <Button
              onClick={handleCreatePayment}
              variant="contained"
              type="submit"
              sx={{ padding: ".8rem 2rem", marginTop: "2rem", width: "100%" }}
            >
              Payment
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default OrderSummary;
