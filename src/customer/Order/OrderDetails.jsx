import React from "react";
import AddressCard from "../AddressCard/AddressCard";
import { Box, Button, Grid } from "@mui/material";
import OrderTraker from "./OrderTracker";
import StarBorderIcon from '@mui/icons-material/StarBorder';
import { Navigate, useNavigate } from "react-router-dom";
import { deepPurple } from "@mui/material/colors";

const OrderDetails = () => {
    const navigate=useNavigate()
//   return (
//     <>
//       <div className="px-5 lg:px-20 m-10 ">
//         <Grid xs={12}>
//           <h1 className="font-bold text-2xl py-7">Delivery Address</h1>
//         </Grid>
//         <div className="">
//           <AddressCard />
//         </div>
//       </div>
//       <section className="py-20">
//         <OrderTraker activeStep={3} />
//       </section>

//       <Grid container className="space-y-5 text-lg">
//       {/* {order.order?.orderItems.map((item) => ( */}
//       <Grid
//         container
//         item
//         className="shadow-xl rounded-md p-5 border"
//         sx={{ alignItems: "center", justifyContent: "space-between",  width: 'calc(100% - 60px)', margin: '0 10px', height: 'auto'}}
//       >
//         <Grid item xs={6}>
//           <div className="flex items-center">
//             <img
//               className="w-[5rem] h-[5rem] object-cover object-top"
//               src="https://www.logoai.com/uploads/output/2023/03/09/3e7b3de38aae120166de3f23d2b59173.jpg"
//               alt=""
//             />
//             <div className="ml-5 space-y-2">
//               <p className="">tit</p>
//               <p className="opacity-50 text-xs font-semibold space-x-5">
//                 <span>Color: pink</span> <span>Size: </span>
//               </p>
//               <p>Seller: </p>
//               <p>₹12 </p>
//             </div>
//           </div>
//         </Grid>
//         <Grid item>
//           <Box
//             sx={{ color: deepPurple[500] }}
//             // onClick={() => navigate(`/account/rate/${item.product._id}`)}
//             className="flex items-center cursor-pointer"
//           >
//             <StarBorderIcon
//               sx={{ fontSize: "3rem" }}
//               className="px-2 text-5xl"
//             />
//             <span>Rate & Review Product</span>
//           </Box>
//         </Grid>
//       </Grid>
      
//     </Grid>
      
//     </>
//   );
return (
    <>
    <div className=" px-2 lg:px-36 space-y-7 ">
      <Grid container className="p-3 shadow-lg">
        <Grid xs={12}>
          <h1 className="font-bold text-lg py-2">Delivery Address</h1>
        </Grid>
        <Grid item xs={6}>
          <AddressCard />
        </Grid>
      </Grid>
      <section className="py-20">
        <OrderTraker activeStep={3} />
      </section>
     
      

    

      <Grid container className="space-y-5">
        {/* {order.order?.orderItems.map((item) => ( */}
          <Grid
            container
            item
            className="shadow-xl rounded-md p-5 border"
            sx={{ alignItems: "center", justifyContent: "space-between" }}
          >
            <Grid item xs={6}>
              {" "}
              <div className="flex  items-center ">
                <img
                  className="w-[5rem] h-[5rem] object-cover object-top"
                //   src={item?.product.imageUrl}
                 src="https://www.logoai.com/uploads/output/2023/03/09/3e7b3de38aae120166de3f23d2b59173.jpg"
                  alt=""
                />
                <div className="ml-5 space-y-2 text-lg">
                  <p className="">title</p>
                  <p className="opacity-50 text-xs font-semibold space-x-5">
                    <span>Color: pink</span> <span>Size: </span>
                  </p>
                  <p>Seller: </p>
                  <p>₹11</p>
                </div>
              </div>
            </Grid>
            <Grid item>
              {
                <Box
                  sx={{ color: deepPurple[500] }}
                //   onClick={() => navigate(`/account/rate/${item.product._id}`)}
                  className="flex items-center cursor-pointer"
                >
                  <StarBorderIcon
                    sx={{ fontSize: "2rem" }}
                    className="px-2 text-2xl"
                    
                  />
                  <span className="text-xl">Rate & Review Product</span>
                </Box>
              }
            </Grid>
          </Grid>
        {/* ))} */}
      </Grid>
      <Box className="p-5 shadow-lg border rounded-md">
        <Grid
          container
          sx={{ justifyContent: "space-between", alignItems: "center" }}
        >
          {/* <Grid item xs={9}>
            <OrderTraker
              activeStep={
                order.order?.orderStatus === "PLACED"
                  ? 1
                  : order.order?.orderStatus === "CONFIRMED"
                  ? 2
                  : order.order?.orderStatus === "SHIPPED"
                  ? 3
                  : 5
              }
            />
          </Grid> */}
          <Grid item>
           <Button sx={{ color: ""}} color="error" variant="text" >
              RETURN
            </Button>

             <Button sx={{ color: deepPurple[500] }} variant="text">
              cancel order
            </Button>
          </Grid>
        </Grid>
      </Box>
        
    </div>
     {/* <BackdropComponent open={order.loading}/> */}
    </>
   
  );
};

export default OrderDetails;
