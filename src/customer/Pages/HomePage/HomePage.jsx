import React from "react";
import Homecarousel from "../../Homecarousel/Homecarousel";
import Homesectioncarousel from "../../Homesectioncarousel/Homesectioncarousel";
import { mens_kurta } from "../../../Data/Men/men_kurta";


const HomePage = () => {
  return (
    <div>
      <Homecarousel />
      <div>
        <Homesectioncarousel data={mens_kurta}  sectioname={"mens curtha"}/>
        <Homesectioncarousel data={mens_kurta}  sectioname={"mens shirt"}/>
        <Homesectioncarousel data={mens_kurta}  sectioname={"women saree"}/>
      </div>
      
    </div>

  );
};

export default HomePage;
