import React from "react";
import "./ProductCard.css";
import { useNavigate } from "react-router-dom";
const ProductCard = ({ product }) => {
  const navigate = useNavigate();

  const handleNavigate=()=>{
    navigate(`/product/${product?._id}`)
  }

  return (
    <div
     onClick={handleNavigate}
      className="productCard w-[20rem] border m-3 transition-all cursor-pointer"
    >
      <div className="h-[20rem]">
        <img
          className="h-full w-full object-cover object-left-top"
          src={product.imageUrl}
          alt={product.title}
        />
      </div>
      <div className="textPart bg-white p-3">
        <div>
          <p className="font-bold opacity-60 text-sm">{product.brand}</p>
          <p className="text-sm">{product.title}</p>
          <p className="font-semibold opacity-50 text-sm">{product.color}</p>
        </div>

        <div className="flex space-x-2 items-center">
          <p className="font-semibold text-sm">₹{product.price}</p>
          <p className="opacity-50 line-through text-sm">
            ₹{product.discountedPrice}
          </p>
          <p className="text-green-600 font-semibold text-sm">
            {product.discountPercent}% off
          </p>
        </div>
      </div>
    </div>
  );
};

export default ProductCard;
