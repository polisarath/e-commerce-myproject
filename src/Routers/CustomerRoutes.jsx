import React from "react";
import { Route, Routes } from "react-router-dom";
import HomePage from "../customer/Pages/HomePage/HomePage";
import Cart from "../customer/Cart/Cart";
import Navigation from "../customer/Navigation/Navigation";
import Footer from "../customer/Footer/Footer";

import ProductDetails from "../customer/ProductDetails/ProductDetails";
import Productlist from "../customer/Product/Productlist";
import CheckOut from "../customer/CheckOut/CheckOut";
import Order from "../customer/Order/Order";
import OrderDetails from "../customer/Order/OrderDetails";
import PaymentSuccess from "../customer/paymentSuccess/PaymentSuccess";

const CustomerRoutes = () => {
  return (
    <>
      <Navigation />
      <Routes>
        <Route path="/login" element={<HomePage />} />
        <Route path="/register" element={<HomePage />} />
        <Route path="/" element={<HomePage />} />
        <Route path="/cart" element={<Cart />} />
        <Route
          path="/:lavelOne/:lavelTwo/:lavelThree"
          element={<Productlist />}
        />
        <Route path="/product/:productId" element={<ProductDetails />} />
        <Route path="/checkout" element={<CheckOut />} />
        <Route path="/account/order" element={<Order />} />
        <Route path="/account/order/:orderId" element={<OrderDetails />} />
        <Route path="/payment/:orderId" element={<PaymentSuccess />}></Route>
        <Route path="*" element={<div>Page not found</div>} />
      </Routes>
      <Footer />
    </>
  );
};

export default CustomerRoutes;
