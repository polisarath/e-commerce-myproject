// import { Route, Routes } from "react-router-dom";
// import "./App.css";
// import CustomerRoutes from "./Routers/CustomerRoutes.jsx";

// function App() {
//   return (
//     <>
//       <Routes>
//         <Route path="/" element={<CustomerRoutes />}></Route>
//       </Routes>
     
      
//     </>
//   );
// }

// export default App;

import React from "react";
import { Route, Routes } from "react-router-dom";
import "./App.css";
import CustomerRoutes from "./Routers/CustomerRoutes";

function App() {
  return (
    <>
      <Routes>
        <Route path="/*" element={<CustomerRoutes />} />
      </Routes>
    </>
  );
}

export default App;
