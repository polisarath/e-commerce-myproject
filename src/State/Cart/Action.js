import axios from "axios";

import api, { API_BASE_URL } from "../../config/apiConfig";
import {
  ADD_ITEM_TO_CART_REQUEST,
  ADD_ITEM_TO_CART_SUCCESS,
  ADD_ITEM_TO_CART_FAILURE,
  GET_CART_FAILURE,
  GET_CART_REQUEST,
  GET_CART_SUCCESS,
  REMOVE_CART_ITEM_FAILURE,
  REMOVE_CART_ITEM_REQUEST,
  REMOVE_CART_ITEM_SUCCESS,
  UPDATE_CART_ITEM_FAILURE,
  UPDATE_CART_ITEM_REQUEST,
  UPDATE_CART_ITEM_SUCCESS,
} from "./ActionType";

// export const addItemToCart = (reqData) => async (dispatch) => {
//   dispatch({ type: ADD_ITEM_TO_CART_REQUEST });
//   try {
//     const { data } = await api.put(`/api/cart/add`, reqData);
//     dispatch({
//       type: ADD_ITEM_TO_CART_SUCCESS,
//       payload: data,
//     });
//     console.log("add item to cart ", data);
//   } catch (error) {
//     dispatch({
//       type: ADD_ITEM_TO_CART_FAILURE,
//       payload:error.message
//     });
//   }
// };

export const addItemToCart = (reqData) => async (dispatch) => {
  dispatch({ type: ADD_ITEM_TO_CART_REQUEST });
  try {
    const { data } = await api.put(`${API_BASE_URL}/api/cart/add`, reqData);
    dispatch({
      type: ADD_ITEM_TO_CART_SUCCESS,
      payload: data,
    });
    console.log("add item to cart ", data);
    return { payload: data }; // return the response
  } catch (error) {
    const errorMessage =
      error.response && error.response.data.message
        ? error.response.data.message
        : error.message;
    dispatch({
      type: ADD_ITEM_TO_CART_FAILURE,
      payload: errorMessage,
    });
    return { error: errorMessage }; // return the error
  }
};

export const getCart = (jwt) => async (dispatch) => {
  dispatch({ type: GET_CART_REQUEST });
  try {
    const { data } = await api.get(`${API_BASE_URL}/api/cart/`);
    dispatch({
      type: GET_CART_SUCCESS,
      payload: data,
    });
    console.log("cart items", data);
  } catch (error) {
    dispatch({
      type: GET_CART_FAILURE,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }
};

// export const removeCartItem = (cartItemId) => async (dispatch) => {
//   try {
//     dispatch({ type: REMOVE_CART_ITEM_REQUEST });
//     await api.delete(
//       `${API_BASE_URL}/api/cart_items/${cartItemId}`
//     );
//     dispatch({
//       type: REMOVE_CART_ITEM_SUCCESS,
//       payload:cartItemId,
//     });
//   } catch (error) {
//     dispatch({
//       type: REMOVE_CART_ITEM_FAILURE,
//       payload:
//         error.response && error.response.data.message
//           ? error.response.data.message
//           : error.message,
//     });
//   }
// };

// export const updateCartItem = (cartItemId) => async (dispatch) => {
//   try {
//     dispatch({ type: UPDATE_CART_ITEM_REQUEST });
//     // const config = {
//     //   headers: {
//     //     Authorization: `Bearer ${reqData.jwt}`,
//     //     "Content-Type": "application/json",
//     //   },
//     // };
//     const { data } = await api.put(
//       `${API_BASE_URL}/api/cart_items/${cartItemId}`
//       // reqData.data,
//       // config
//     );
//     console.log("udated cartitem ", data);
//     dispatch({
//       type: UPDATE_CART_ITEM_SUCCESS,
//       payload: data,
//     });
//   } catch (error) {
//     dispatch({
//       type: UPDATE_CART_ITEM_FAILURE,
//       payload:
//         error.response && error.response.data.message
//           ? error.response.data.message
//           : error.message,
//     });
//   }
// };

export const removeCartItem = (cartItemId) => async (dispatch) => {
  dispatch({ type: REMOVE_CART_ITEM_REQUEST });
  try {
    await api.delete(`${API_BASE_URL}/api/cart_items/${cartItemId}`);
    dispatch({
      type: REMOVE_CART_ITEM_SUCCESS,
      payload: cartItemId,
    });
  } catch (error) {
    dispatch({
      type: REMOVE_CART_ITEM_FAILURE,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }
};

export const updateCartItem = (data) => async (dispatch) => {
  dispatch({ type: UPDATE_CART_ITEM_REQUEST });
  try {
    const response = await api.put(
      `/api/cart_items/${data.CartItemId}`,
      data.data
    );
    console.log("Updated cart item:", response.data);
    dispatch({
      type: UPDATE_CART_ITEM_SUCCESS,
      payload: response.data,
    });
  } catch (error) {
    dispatch({
      type: UPDATE_CART_ITEM_FAILURE,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }
};
